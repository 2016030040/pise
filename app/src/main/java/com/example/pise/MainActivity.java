package com.example.pise;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity  {

    private Button btnPerfil;
    private Button btnActvividadActual;
    private Button btnCursos;
    private Button btnVacantes;

    Usuario usu;
    private Intent i;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnPerfil = (Button) findViewById(R.id.btnMiPerfil);
        btnActvividadActual = (Button) findViewById(R.id.btnActvividadActual);
        btnCursos = (Button) findViewById(R.id.btnCursos);
        btnVacantes = (Button) findViewById(R.id.btnVacantes);

        usu = new Usuario();
        i = this.getIntent();
        bundle = i.getExtras();
        usu = (Usuario) bundle.getSerializable("usuario");

        btnPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PerfilActivity.class);
                Bundle Object= new Bundle();
                Object.putSerializable("usuario",usu);
                intent.putExtras(Object);
                startActivity(intent);
            }
        });

        btnActvividadActual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ActividadActualActivity.class);
                Bundle Object= new Bundle();
                Object.putSerializable("usuario",usu);
                intent.putExtras(Object);
                startActivity(intent);

            }
        });

        btnCursos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CursosActivity.class);
                Bundle Object= new Bundle();
                Object.putSerializable("usuario",usu);
                intent.putExtras(Object);
                startActivity(intent);
            }
        });


        btnVacantes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, VacantesActivity.class);
                startActivityForResult(i, 0);
            }
        });
    }
}
