package com.example.pise.DataBase;
import android.content.Context;
import android.util.Log;
import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.Cursos;
import com.example.pise.Usuario;

import org.json.JSONObject;




public class ProcesosPHP implements Response.ErrorListener, Response.Listener<JSONObject> {


    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private JsonArrayRequest jsonArrayRequest;
    private String serverip = "https://pise-android.000webhostapp.com/WebService/";

    public void setContext(Context context) {
        request = Volley.newRequestQueue(context);
    }

    /*public void wsConfirmarAsistencia(Usuario u, int idCurso) {
        String url = getServerip() + "wsConfirmarAsistencia.php?idUsuario=" + u.getIdUsuario() + "&idCurso=" + idCurso;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }*/


    //Joyce
    public void wsConsultarPerfil(Usuario u) {
        String url = getServerip() + "perfil.php?idusuario=" + u.getIdUsuario();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    //Joyce
    public void wsModificarContacto(Usuario u) {
        String url = getServerip() + "actualizarPerfil.php?idUsuario=" + u.getIdUsuario()
                + "&telefono=" + u.getTelefono()
                + "&celular=" + u.getCelular()
                + "&email=" + u.getEmail()
                + "&emailAlterno=" + u.getEmailAlterno();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    //Pendiente clase de curso Melanie
    public void wsConsultarCursoActivo(Cursos c) {
        String url = getServerip() + "wsConsultarCursoActivo.php?idCurso=" + c.getId();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    //Pendiente clase de curso Melanie
    public void wsConsultarCursos() {
        String url = getServerip() + "wsConsultarCursos.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    public void wsConfirmarAsistencia(String idUsuario, int idCurso){
        String url = getServerip() + "wsConfirmarAistencia.php?idUsuario=" + idUsuario + "&idCurso=" + idCurso;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    //Muestra el puesto de trabajo, fecha inicio Oscar
    public void wsConsultarLaboral(Usuario u) {
        String url = getServerip() + "wsConsultarLaboral.php?idUsuario=" + u.getIdUsuario();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    //Oscar
    public void wsConsultarPosgrado(Usuario u) {
        String url = getServerip() + "wsConsultarPosgrado.php?idUsuario=" + u.getIdUsuario();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }


    //Falta la clase vacantes Liliana
    public void wsConsultarVacante(int idVacante) {
        String url = getServerip() + "wsConsultarVacante.php?idVacante=" + idVacante;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }
    //Falta la clase vacantes Liliana
    public void wsConsultarVacantes() {
        String url = getServerip() + "wsConsultarVacantes.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }


    //Oscar
    public void wsModificarLaboral(Usuario u) {
        String url = getServerip() + "wsModificarLaboral.php?idUsuario=" + u.getIdUsuario()
                + "&trabajo=" + u.getEmpresa()
                + "&puesto=" + u.getPuesto()
                + "&fecha_inicio=" + u.getFechaIngreso();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    //Oscar
    public void wsModificarPosgrado(Usuario u) {
        String url = getServerip() + "wsModificarPosgrado.php?idUsuario=" + u.getIdUsuario()
                + "&nombre=" + u.getNombrePos()
                + "&escuela=" + u.getEscuelaPos()
                + "&deseo=" + u.getQuieroPos();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }


    /*public void wsRecuperarContraseña(Usuario c) {
        String url = getServerip() + "wsRecuperarContraseña.php?user=" + c.getNombre();
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }*/

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR", error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {

    }

    public String getServerip() {
        return serverip;
    }

    public void setServerip(String serverip) {
        this.serverip = serverip;
    }


}
