package com.example.pise;

public class Cursos {

    private int id;
    private String nombre;
    private String impartida;
    private String diaHora;
    private int asistencia;

    public Cursos(int id, String nombre, String impartida, String diaHora, int asistencia) {
        this.id = id;
        this.nombre = nombre;
        this.impartida = impartida;
        this.diaHora = diaHora;
        this.asistencia = asistencia;
    }

    public Cursos() {
        this.setId(0);
        this.setNombre("");
        this.setImpartida("");
        this.setDiaHora("");
        this.setAsistencia(0);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImpartida() {
        return impartida;
    }

    public void setImpartida(String impartida) {
        this.impartida = impartida;
    }

    public String getDiaHora() {
        return diaHora;
    }

    public void setDiaHora(String diaHora) {
        this.diaHora = diaHora;
    }

    public int getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(int asistencia) {
        this.asistencia = asistencia;
    }
}
