package com.example.pise;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VacantesActivity extends FragmentActivity implements Response.ErrorListener, Response.Listener<JSONObject> {

    ListView lista; //listavacantes
    ArrayList<Vacantes> listaVacantes; //vacantes
    private VacantesActivity.MyArrayAdapter adapter;
    private String serverip = "https://pise-android.000webhostapp.com/WebService/";
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    TabHost tabs;
    private TextView txtPuesto;
    private TextView txtEmpresa;
    private TextView txtRequisitos;
    public int id = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacantes);

        lista = findViewById(R.id.lvVacantes);
        request = Volley.newRequestQueue(VacantesActivity.this);

        listaVacantes = new ArrayList<Vacantes>();

        txtEmpresa = (TextView) findViewById(R.id.txtEmpresa);
        txtPuesto = (TextView) findViewById(R.id.txtPuesto);
        txtRequisitos = (TextView) findViewById(R.id.txtRequisitos);

        wsConsultarVacantes();

        tabs = (TabHost) findViewById(android.R.id.tabhost);
        tabs.setup();
        //Asignar la tab1 que es la de Curso Activo
        TabHost.TabSpec spec = tabs.newTabSpec("tab1");
        spec.setContent(R.id.tabs1);
        spec.setIndicator("Vacantes");
        tabs.addTab(spec);
        //Asignar la tab2 que es para la lista de cursos
        spec = tabs.newTabSpec("tab2");
        spec.setContent(R.id.tabs2);
        spec.setIndicator("Lista vacantes");
        tabs.addTab(spec);

        tabs.setCurrentTab(0); //Para que se ponga la primer tab en la posicion 0


    }

    public void wsConsultarVacantes() {
        try {
            String url = serverip + "wsConsultarVacantes.php";
            url = url.replace(" ", "%20");
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(VacantesActivity.this,""+ jsonObjectRequest,Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

        Vacantes c = null;
        JSONArray json = response.optJSONArray("vacantes");
        try {
            for(int i=0;i<json.length();i++){
                c = new Vacantes();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                c.setId(jsonObject.optInt("id"));
                c.setEmpresa(jsonObject.optString("nombre_empresa"));
                c.setRequisitos(jsonObject.optString("descripcion"));
                c.setPuesto(jsonObject.optString("nombre"));

                listaVacantes.add(c);
            }

            adapter = new VacantesActivity.MyArrayAdapter(VacantesActivity.this, R.layout.layout_vacantes, listaVacantes);
            lista.setAdapter(adapter);


            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Vacantes vac = listaVacantes.get(i);
                    id = vac.getId(); // aqui jalo el id cuando le doy click al item o curso
                    wsConsultarVacanteSeleccionada();
                }
            });

        } catch (JSONException e) {
            //Toast.makeText(getContext(),"Valio queso",Toast.LENGTH_LONG).show();
            e.printStackTrace();

        }

    }

    class MyArrayAdapter extends ArrayAdapter<Vacantes> {
        Context context;
        int textViewRecursoId;
        ArrayList<Vacantes> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Vacantes> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView txtPuesto = (TextView) view.findViewById(R.id.txtPuesto);
            TextView txtEmpresa = (TextView) view.findViewById(R.id.txtEmpresa);
            TextView txtRequisitos = (TextView) view.findViewById(R.id.txtRequisitos);

            txtPuesto.setText(objects.get(position).getPuesto());
            txtEmpresa.setText(objects.get(position).getEmpresa());
            return view;

        }

    }

    public void wsConsultarVacanteSeleccionada()
    {
        String url = serverip + "wsConsultarVacante.php?idVacante=" + id;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray json = response.optJSONArray("vacantes");
                        try
                        {
                            JSONObject jsonObject = null;
                            jsonObject = json.getJSONObject(0);
                            txtEmpresa.setText(jsonObject.optString("nombre_empresa"));
                            txtPuesto.setText(jsonObject.optString("nombre"));
                            txtRequisitos.setText(jsonObject.optString("descripcion"));


                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        request.add(jsonObjectRequest);
        tabs.setCurrentTab(0);
    }



}
