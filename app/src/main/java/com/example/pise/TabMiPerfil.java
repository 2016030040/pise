package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.squareup.picasso.Picasso;

public class TabMiPerfil extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener{
    private ImageView imgPerfil;
    private TextView txtNombre;
    private TextView txtCarrera;
    private TextView txtNacimiento;
    private TextView txtSexo;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private String url;
    private String server = "https://pise-android.000webhostapp.com/WebService/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_mi_perfil, container, false);

        imgPerfil = (ImageView) view.findViewById(R.id.imgPerfil);
        txtNombre = (TextView) view.findViewById(R.id.txtNombre);
        txtCarrera = (TextView) view.findViewById(R.id.txtCarrera);
        txtNacimiento = (TextView) view.findViewById(R.id.txtNacimiento);
        txtSexo = (TextView) view.findViewById(R.id.txtSexo);
        request = Volley.newRequestQueue(view.getContext());

        int id = getArguments().getInt("id");

        Usuario usu = new Usuario();

        usu.setIdUsuario(String.valueOf(id));

        wsConsultarPerfil(usu);

        return view;
    }

    public void wsConsultarPerfil(Usuario usu) {
        try {
            String url = server + "perfil.php?idAlumno=" + usu.getIdUsuario().trim();
            Log.d("url:", url);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("usuarios");
        try {
            for (int i=0;i<json.length();i++){
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                txtNombre.setText(jsonObject.optString("0") + " " + jsonObject.optString("1") + " " + jsonObject.optString("2"));
                txtCarrera.setText(jsonObject.optString("3"));
                txtNacimiento.setText(jsonObject.optString("4"));
                txtSexo.setText(jsonObject.optString("6"));
                url=jsonObject.optString("5");
                if (url.isEmpty()){
                    imgPerfil.setImageResource(R.drawable.user_icon);
                }else {
                    Picasso.with(getContext()).load(url).into(imgPerfil);
                }
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }
}