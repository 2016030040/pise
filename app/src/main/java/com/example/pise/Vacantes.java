package com.example.pise;

public class Vacantes {


    private int id;
    private String puesto;
    private String empresa;
    private String requisitos;


    public Vacantes(int id,String puesto, String empresa, String requisitos) {
        this.setId(id);
        this.setPuesto(puesto);
        this.setEmpresa(empresa);
        this.setRequisitos(requisitos);
    }

    public  Vacantes (){
        this.setId(0);
        this.setPuesto("");
        this.setEmpresa("");
        this.setRequisitos("");

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(String requisitos) {
        this.requisitos = requisitos;
    }
}

