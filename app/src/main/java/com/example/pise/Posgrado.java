package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Posgrado extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener,View.OnClickListener {

    private TextView lblNombrePosgrado;
    private EditText txtNombrePosgrado;
    private EditText txtEscuela;
    private RadioGroup rdgPregunta;
    private RadioButton rdbSi;
    private RadioButton rdbNo;
    private Button btnActualizarPosgrado;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private String serverip = "https://pise-android.000webhostapp.com/WebService/";
    ProcesosPHP procesosPHP = new ProcesosPHP();
    Usuario usu = new Usuario();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_posgrado, container, false);

        lblNombrePosgrado = (TextView) view.findViewById(R.id.lblNombrePosgrado);
        txtNombrePosgrado = (EditText) view.findViewById(R.id.txtNombrePosgrado);
        txtEscuela = (EditText) view.findViewById(R.id.txtEscuela);
        rdgPregunta = (RadioGroup) view.findViewById(R.id.rdgPregunta);
        rdbSi = (RadioButton) view.findViewById(R.id.rdbSi);
        rdbNo = (RadioButton) view.findViewById(R.id.rdbNo);
        btnActualizarPosgrado = (Button) view.findViewById(R.id.btnActualizarPosgrado);

        request = Volley.newRequestQueue(view.getContext());

        int id = getArguments().getInt("id");


        procesosPHP.setContext(view.getContext());


        usu.setIdUsuario(String.valueOf(id));
        this.btnActualizarPosgrado.setOnClickListener(this);
        wsConsultarPosgrado(usu);

        /*btnActualizarPosgrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtEscuela.getText().toString().trim().isEmpty() || txtNombrePosgrado.getText().toString().trim().isEmpty() &&
                        (rdbSi.isChecked() || rdbNo.isChecked())){
                    Toast.makeText(view.getContext(), "Hay campos sin llenar", Toast.LENGTH_SHORT).show();
                }else {
                    usu.setNombrePos(txtNombrePosgrado.getText().toString());
                    usu.setEscuelaPos(txtEscuela.getText().toString());
                    if (rdbSi.isChecked()){
                        usu.setQuieroPos(1);
                    }else if (rdbNo.isChecked()){
                        usu.setQuieroPos(0);
                    }
                    procesosPHP.wsModificarPosgrado(usu);
                    Toast.makeText(view.getContext(), "Datos modificados", Toast.LENGTH_SHORT).show();
                }
                wsConsultarPosgrado(usu);
            }
        });*/

        return view;
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

        Usuario usuario = null;
        final JSONArray json = response.optJSONArray("Trabajos");
        try {
            for (int i=0;i<json.length();i++){
                usuario = new Usuario();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                lblNombrePosgrado.setText(jsonObject.optString("0"));
                txtNombrePosgrado.setText(jsonObject.optString("1"));
                txtEscuela.setText(jsonObject.optString("2"));

                if(jsonObject.optString("3").equals("1")){
                    rdgPregunta.check(rdbSi.getId());
                }else {
                    rdgPregunta.check(rdbNo.getId());
                }
            }

        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    public void wsConsultarPosgrado(Usuario usu) {
        try {
            String url = serverip + "wsConsultarPosgrado.php?idUsuario=" + usu.getIdUsuario().trim();
            Log.d("url:", url);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnActualizarPosgrado:
                boolean completo = true;
                if (txtNombrePosgrado.getText().toString().equals("")){
                    txtNombrePosgrado.setError("Introduce un posgrado");
                    completo = false;
                }
                if (txtEscuela.getText().toString().equals("")){
                    txtEscuela.setError("Introduce una escuela");
                    completo = false;
                }
                /*if (rdgPregunta.getCheckedRadioButtonId() == -1){
                    rdgPregunta.setError("Introduce una fecha");
                    completo = false;
                }*/
                if (completo){
                    if ((txtNombrePosgrado.getText().toString().equals((usu.getNombrePos()))&&
                            (txtEscuela.getText().toString().equals(usu.getEscuelaPos())) &&
                            ((rdbSi.isChecked() && usu.getQuieroPos()==1) || (rdbNo.isChecked() && usu.getQuieroPos()==0))
                    ))
                    {
                        Toast.makeText(view.getContext(), "No se modificó nada", Toast.LENGTH_SHORT).show();
                    } /*else if ((rdbSi.isChecked() && usu.getQuieroPos()==1) || (rdbNo.isChecked() && usu.getQuieroPos()==2)) {
                        Toast.makeText(view.getContext(), "No se modificó nada", Toast.LENGTH_SHORT).show();
                    }*/
                    else {
                        usu.setNombrePos(txtNombrePosgrado.getText().toString());
                        usu.setEscuelaPos(txtEscuela.getText().toString());
                        if (rdbSi.isChecked()){
                            usu.setQuieroPos(1);
                        } else if (rdbNo.isChecked()) {
                            usu.setQuieroPos(0);
                        }

                        procesosPHP.wsModificarPosgrado(usu);
                        Toast.makeText(view.getContext(), "Se modificó correctamente", Toast.LENGTH_SHORT).show();
                    }
                    wsConsultarPosgrado(usu);
                }
                break;
        }
    }
}
