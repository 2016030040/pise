package com.example.pise;

import java.io.Serializable;

public class Usuario implements Serializable {
    private String idUsuario;
    private String idPerfil;
    private String usuario;
    private String password;


    private String status;
    //mi perfil
    private String urlFoto;
    private String nombre;
    private String carrera;
    private String fechaNac;
    private String sexo;
    //contacto
    private String telefono;
    private String celular;
    private String email;
    private String emailAlterno;
    //Actividad actual (Laboral)
    private String puesto;
    private String empresa;
    private String fechaIngreso;
    //Posgrado
    private String nombrePos;
    private String escuelaPos;
    private int quieroPos;


    public Usuario(String idUsuario, String idPerfil, String usuario, String password, String status, String urlFoto, String nombre, String carrera, String fechaNac, String sexo, String telefono, String celular, String email, String emailAlterno, String puesto, String empresa, String fechaIngreso, String nombrePos, String escuelaPos, int quieroPos) {
        this.idUsuario = idUsuario;
        this.idPerfil = idPerfil;
        this.usuario = usuario;
        this.password = password;
        this.status = status;
        this.urlFoto = urlFoto;
        this.nombre = nombre;
        this.carrera = carrera;
        this.fechaNac = fechaNac;
        this.sexo = sexo;
        this.telefono = telefono;
        this.celular = celular;
        this.email = email;
        this.emailAlterno = emailAlterno;
        this.puesto = puesto;
        this.empresa = empresa;
        this.fechaIngreso = fechaIngreso;
        this.nombrePos = nombrePos;
        this.escuelaPos = escuelaPos;
        this.quieroPos = quieroPos;
    }

    public Usuario() {
        this.setIdUsuario("");
        this.setIdPerfil("");
        this.setUsuario("");
        this.setPassword("");
        this.setStatus("");
        this.setUrlFoto("");
        this.setNombre("");
        this.setCarrera("");
        this.setFechaNac("");
        this.setSexo("");
        this.setTelefono("");
        this.setCelular("");
        this.setEmail("");
        this.setEmailAlterno("");
        this.setPuesto("");
        this.setEmpresa("");
        this.setFechaIngreso("");
        this.setNombrePos("");
        this.setEscuelaPos("");
        this.setQuieroPos(0);


    }

    public String getEmailAlterno() {
        return emailAlterno;
    }

    public void setEmailAlterno(String emailAlterno) {
        this.emailAlterno = emailAlterno;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNombrePos() {
        return nombrePos;
    }

    public void setNombrePos(String nombrePos) {
        this.nombrePos = nombrePos;
    }

    public String getEscuelaPos() {
        return escuelaPos;
    }

    public void setEscuelaPos(String escuelaPos) {
        this.escuelaPos = escuelaPos;
    }

    public int getQuieroPos() {
        return quieroPos;
    }

    public void setQuieroPos(int quieroPos) {
        this.quieroPos = quieroPos;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdPerfil() {
        return idPerfil;
    }

    public void setIdPerfil(String idPerfil) {
        this.idPerfil = idPerfil;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }
}