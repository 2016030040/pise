package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Laboral extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener,View.OnClickListener{

    private TextView lblNombreLaboral;
    private TextView lblUltimoEmpleoPuesto;
    private TextView lblUltimoEmpleoEmpresa;
    private EditText txtPuesto;
    private EditText txtEmpresa;
    private EditText txtFechaIngreso;
    private Button btnActualizarEmpleado;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private String serverip = "https://pise-android.000webhostapp.com/WebService/";
    Usuario usu = new Usuario();
    ProcesosPHP procesosPHP = new ProcesosPHP();


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_laboral, container, false);

        lblNombreLaboral = (TextView) view.findViewById(R.id.lblNombreLaboral);
        lblUltimoEmpleoPuesto = (TextView) view.findViewById(R.id.lblUltimoEmpleoPuesto);
        lblUltimoEmpleoEmpresa = (TextView) view.findViewById(R.id.lblUltimoEmpleoEmpresa);
        txtPuesto = (EditText) view.findViewById(R.id.txtPuesto);
        txtEmpresa = (EditText) view.findViewById(R.id.txtEmpresa);
        txtFechaIngreso = (EditText) view.findViewById(R.id.txtFechaIngreso);
        btnActualizarEmpleado = (Button) view.findViewById(R.id.btnActualizarEmpleado);

        request = Volley.newRequestQueue(view.getContext());

        int id = getArguments().getInt("id");

        procesosPHP.setContext(view.getContext());

        usu.setIdUsuario(String.valueOf(id));
        this.btnActualizarEmpleado.setOnClickListener(this);
        wsConsultarLaboral(usu);

        /*btnActualizarEmpleado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtEmpresa.getText().toString().trim().isEmpty() || txtPuesto.getText().toString().trim().isEmpty() ||
                        txtFechaIngreso.getText().toString().trim().isEmpty()){
                    Toast.makeText(view.getContext(), "Hay campos sin llenar", Toast.LENGTH_SHORT).show();
                } else {
                    usu.setPuesto(txtPuesto.getText().toString());
                    usu.setEmpresa(txtEmpresa.getText().toString());
                    usu.setFechaIngreso(txtFechaIngreso.getText().toString());

                    procesosPHP.wsModificarLaboral(usu);
                    Toast.makeText(view.getContext(), "Datos modificados", Toast.LENGTH_SHORT).show();
                    limpiar();
                }
                wsConsultarLaboral(usu);
            }
        });*/

        return view;
    }



    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse( JSONObject response) {

        final JSONArray json = response.optJSONArray("Trabajos");
        try {
            for (int i=0;i<json.length();i++){
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                lblUltimoEmpleoEmpresa.setText(jsonObject.optString("0"));
                lblUltimoEmpleoPuesto.setText(jsonObject.optString("1"));
                lblNombreLaboral.setText(jsonObject.optString("3"));
            }

        }catch (JSONException e){
            e.printStackTrace();
        }


    }

    public void wsConsultarLaboral(Usuario usu) {
        try {
            String url = serverip + "wsConsultarLaboral.php?idUsuario=" + usu.getIdUsuario().trim();
            Log.d("url:", url);
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public  void  limpiar()
    {
        txtEmpresa.setText("");
        txtPuesto.setText("");
        txtFechaIngreso.setText("");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnActualizarEmpleado:
                boolean completo = true;
                if (txtPuesto.getText().toString().equals("")){
                    txtPuesto.setError("Introduce un puesto");
                    completo = false;
                }
                if (txtEmpresa.getText().toString().equals("")){
                    txtEmpresa.setError("Introduce una empresa");
                    completo = false;
                }
                if (txtFechaIngreso.getText().toString().equals("")){
                    txtFechaIngreso.setError("Introduce una fecha");
                    completo = false;
                }
                if (completo){
                    if ((txtPuesto.getText().toString().equals((usu.getPuesto()))&&
                            (txtEmpresa.getText().toString().equals(usu.getEmpresa())) &&
                            (txtFechaIngreso.getText().toString().equals(usu.getFechaIngreso()))))
                    {
                        Toast.makeText(view.getContext(), "No se modificó nada", Toast.LENGTH_SHORT).show();
                    }else {
                        usu.setPuesto(txtPuesto.getText().toString());
                        usu.setEmpresa(txtEmpresa.getText().toString());
                        usu.setFechaIngreso(txtFechaIngreso.getText().toString());

                        procesosPHP.wsModificarLaboral(usu);
                        Toast.makeText(view.getContext(), "Se modificó correctamente", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }
                    wsConsultarLaboral(usu);
                }
                break;
        }
    }
}

