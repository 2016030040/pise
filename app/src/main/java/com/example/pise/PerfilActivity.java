package com.example.pise;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

public class PerfilActivity extends FragmentActivity {

    public FragmentTabHost tabHost;
    private Intent i;
    private Bundle bundle;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);

        i = this.getIntent();
        bundle = i.getExtras();
        usuario = new Usuario();
        usuario = (Usuario) bundle.getSerializable("usuario");
        Bundle args = new Bundle();

        args.putInt("id",Integer.parseInt(usuario.getIdUsuario()));

        tabHost= findViewById(android.R.id.tabhost);
        tabHost.setup(this,getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab_mi_perfil").setIndicator("Mi Perfil"),TabMiPerfil.class, args);
        tabHost.addTab(tabHost.newTabSpec("tab_contacto").setIndicator("Contacto"), TabContacto.class, args);
    }
}