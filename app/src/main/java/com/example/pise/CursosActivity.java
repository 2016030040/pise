package com.example.pise;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTabHost;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CursosActivity extends FragmentActivity implements Response.ErrorListener, Response.Listener<JSONObject> {

    ListView lista; //listacursos
    ArrayList<Cursos> listaCursos; //cursos
    ProcesosPHP php = new ProcesosPHP();
    private CursosActivity.MyArrayAdapter adapter;
    private String serverip = "https://pise-android.000webhostapp.com/WebService/";
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    TabHost tabs;
    private TextView txtNombre;
    private TextView txtImpartida;
    private TextView txtDia;
    private Button btnEnviar;
    private RadioButton rdbSi;
    private RadioButton rdbNo;
    public int id = 0;

    //mandar el usuario
    private Intent i;
    private Bundle bundle;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cursos);

        i = this.getIntent();
        bundle = i.getExtras();
        usuario = new Usuario();
        usuario = (Usuario) bundle.getSerializable("usuario");
        Bundle args = new Bundle();

        args.putInt("id",Integer.parseInt(usuario.getIdUsuario()));

        lista = findViewById(R.id.lvCursos);
        request = Volley.newRequestQueue(CursosActivity.this);

        listaCursos = new ArrayList<Cursos>();

        txtNombre = (TextView) findViewById(R.id.txtNombre);
        txtImpartida = (TextView) findViewById(R.id.txtImpartida);
        txtDia = (TextView) findViewById(R.id.txtDia);
        btnEnviar = (Button) findViewById(R.id.btnEnviar);
        rdbSi = (RadioButton) findViewById(R.id.rdbSi);
        rdbNo = (RadioButton) findViewById(R.id.rdbNo);
        wsConsultarCursos();

        tabs = (TabHost) findViewById(android.R.id.tabhost);
        tabs.setup();
        //Asignar la tab1 que es la de Curso Activo
        TabHost.TabSpec spec = tabs.newTabSpec("tab1");
        spec.setContent(R.id.tabs1);
        spec.setIndicator("Curso Activo");
        tabs.addTab(spec);
        //Asignar la tab2 que es para la lista de cursos
        spec = tabs.newTabSpec("tab2");
        spec.setContent(R.id.tabs2);
        spec.setIndicator("Curso Activo");
        tabs.addTab(spec);

        tabs.setCurrentTab(0); //Para que se ponga la primer tab en la posicion 0


    }

    public void wsConsultarCursos() {
        String url = serverip + "wsConsultarCursos.php";
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {

        Cursos c = null;
        JSONArray json = response.optJSONArray("cursos");
        try {
            for(int i=0;i<json.length();i++){
                c = new Cursos();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                c.setId(jsonObject.optInt("idcursos"));
                c.setNombre(jsonObject.optString("nomCurso"));
                c.setImpartida(jsonObject.optString("nombreUni"));

                listaCursos.add(c);
            }

            CursosActivity.MyArrayAdapter adapter = new CursosActivity.MyArrayAdapter(CursosActivity.this,R.layout.layout_cursos,listaCursos);
            lista.setAdapter(adapter);

            lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Cursos cur = listaCursos.get(i);
                    id = cur.getId(); // aqui jalo el id cuando le doy click al item o curso
                    wsConsultarCursoActivos();
                }
            });

        } catch (JSONException e) {
            //Toast.makeText(getContext(),"Valio queso",Toast.LENGTH_LONG).show();
            e.printStackTrace();

        }

    }

    class MyArrayAdapter extends ArrayAdapter<Cursos> {
        Context context;
        int textViewRecursoId;
        ArrayList<Cursos> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Cursos> objects) {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewRecursoId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewRecursoId, null);

            TextView txtNombre = (TextView) view.findViewById(R.id.txtNombre);
            TextView txtUniversidad = (TextView) view.findViewById(R.id.txtUniversidad);

            txtNombre.setText(objects.get(position).getNombre());
            txtUniversidad.setText("Impartido por: " + objects.get(position).getImpartida());

            return view;

        }

    }

    public void wsConsultarCursoActivos()
    {
        String url = serverip + "wsConsultarCursoActivo.php?idCurso=" + id;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray json = response.optJSONArray("cursos");
                        try
                        {
                            JSONObject jsonObject = null;
                            jsonObject = json.getJSONObject(0);
                            txtNombre.setText(jsonObject.optString("nomCurso"));
                            txtImpartida.setText(jsonObject.optString("nombreUni"));
                            txtDia.setText(jsonObject.optString("fechacurso"));

                            btnEnviar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    wsConfirmarAsis(usuario.getIdUsuario(),id);
                                    Toast.makeText(CursosActivity.this,"Se ha registrado exitosamente.",Toast.LENGTH_LONG).show();
                                }
                            });

                        }catch (JSONException e)
                        {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        request.add(jsonObjectRequest);
        tabs.setCurrentTab(0);
    }

    public void wsConfirmarAsis(String idUsuario, int idCurso)
    {
        String url = serverip + "wsConfirmarAsistencia.php?idUsuario=" + idUsuario + "&idCurso=" + idCurso;
        url = url.replace(" ", "%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {



                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        request.add(jsonObjectRequest);
        tabs.setCurrentTab(0);
    }


}
