package com.example.pise;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

public class LoginActivity extends AppCompatActivity implements Response.ErrorListener, Response.Listener<JSONObject> {

    private Button btnIngresar;
    private Button btnOlvidaste;
    private TextView txtUsuario;
    private TextView txtContrasena;
    private Usuario usuario;

    private RequestQueue requestQueue;
    private JsonObjectRequest jsonObjectRequest;

    private final Context context = LoginActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        requestQueue = Volley.newRequestQueue(LoginActivity.this);

        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnOlvidaste = (Button) findViewById(R.id.btnOlvidaste);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        txtContrasena = (TextView) findViewById(R.id.txtContrasena);

        btnIngresar.setEnabled(true);

        usuario = new Usuario();
        requestQueue = Volley.newRequestQueue(context);
        if ( isNetworkAvailable())
        {
            btnIngresar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!txtUsuario.getText().toString().isEmpty() && !txtContrasena.getText().toString().isEmpty())
                    {
                        btnIngresar.setEnabled(false);
                        consultarUsuario();
                    }
                    else
                        Toast.makeText(LoginActivity.this, "Para ingresar son requeridos Contraseña y Usuario", Toast.LENGTH_SHORT).show();
                }
            });
            btnOlvidaste.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent browserIntent = new Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://siiaa.mx/index.php/login/recuperacion_contrasena"));
                    startActivity(browserIntent);
                }
            });

        }
    }

    public void consultarUsuario(){
        byte[] md5Input = txtContrasena.getText().toString().getBytes();
        BigInteger md5Data = null;

        try {
            md5Data = new BigInteger(1, md5.encryptMD5(md5Input));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String url = "https://pise-android.000webhostapp.com/WebService/login.php?usuario=" + txtUsuario.getText().toString()+
                "&password="+md5Data;
        Log.i("Password", url);

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        requestQueue.add(jsonObjectRequest);
    }

    @Override

    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this,"Error en la consulta", Toast.LENGTH_SHORT).show();
        btnIngresar.setEnabled(true);
        Toast.makeText(getApplicationContext(),
                error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(JSONObject response) {
        try{

            JSONArray jsonArray = response.optJSONArray("usuarios");
            JSONObject jsonObject = null;
            jsonObject = jsonArray.getJSONObject(0);

            if(jsonArray!=null){

                usuario.setIdUsuario(jsonObject.optString("idusuario"));
                usuario.setIdPerfil(jsonObject.optString("id_perfil"));
                usuario.setUsuario(jsonObject.optString("usuario"));
                usuario.setPassword(jsonObject.optString("password"));
                usuario.setFechaIngreso(jsonObject.optString("fecharegistro"));
                usuario.setEmail(jsonObject.optString("email"));
                usuario.setStatus(jsonObject.optString("status"));
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                Bundle object= new Bundle();
                object.putSerializable("usuario", usuario);

                i.putExtras(object);
                startActivity(i);
                finish();
            } else{
                Toast.makeText(this,"Usuario o contraseña incorrectos",Toast.LENGTH_LONG).show();
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
}