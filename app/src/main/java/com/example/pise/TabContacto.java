package com.example.pise;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.pise.DataBase.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TabContacto extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener, View.OnClickListener{
    private EditText txtTelefono;
    private EditText txtMovil;
    private EditText txtCorreo;
    private EditText txtCorreoAlt;
    private Button btnActualizarDatos;
    private JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    Usuario usu = new Usuario();
    ProcesosPHP php = new ProcesosPHP();
    private String server = "https://pise-android.000webhostapp.com/WebService/";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_contacto, container, false);

        txtTelefono = (EditText) view.findViewById(R.id.txtTelefono);
        txtMovil = (EditText) view.findViewById(R.id.txtMovil);
        txtCorreo = (EditText) view.findViewById(R.id.txtCorreo);
        txtCorreoAlt = (EditText) view.findViewById(R.id.txtCorreoAlt);
        btnActualizarDatos = (Button) view.findViewById(R.id.btnActualizarDatos);
        request = Volley.newRequestQueue(view.getContext());

        int id = getArguments().getInt("id");

        php.setContext(view.getContext());

        usu.setIdUsuario(String.valueOf(id));
        this.btnActualizarDatos.setOnClickListener(this);
        wsConsultarContacto(usu);



        return view;
    }
    public void wsConsultarContacto(Usuario usu) {
        try {
            String url = server + "consultarContacto.php?idusuario=" + usu.getIdUsuario().trim();
            jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
            request.add(jsonObjectRequest);
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnActualizarDatos:
                boolean completo = true;
                if (txtTelefono.getText().toString().equals("")){
                    txtTelefono.setError("Introduce un telefono");
                    completo = false;
                }
                if (txtMovil.getText().toString().equals("")){
                    txtMovil.setError("Introduce un telefono movil");
                    completo = false;
                }
                if (txtCorreo.getText().toString().equals("")){
                    txtCorreo.setError("Introduce un correo");
                    completo = false;
                }
                if (txtCorreoAlt.getText().toString().equals("")){
                    txtCorreoAlt.setError("Introduce un correo alterno");
                    completo = false;
                }
                if (completo){
                    if ((txtTelefono.getText().toString().equals((usu.getTelefono()))&&
                            (txtMovil.getText().toString().equals(usu.getCelular())) &&
                            (txtCorreo.getText().toString().equals(usu.getEmail())) &&
                            (txtCorreoAlt.getText().toString().equals(usu.getEmailAlterno()))))
                    {
                        Toast.makeText(view.getContext(), "No se modifico nada", Toast.LENGTH_SHORT).show();
                    }else {
                        usu.setTelefono(txtTelefono.getText().toString());
                        usu.setCelular(txtMovil.getText().toString());
                        usu.setEmail(txtCorreo.getText().toString());
                        usu.setEmailAlterno(txtCorreoAlt.getText().toString());

                        php.wsModificarContacto(usu);
                        Toast.makeText(view.getContext(), "Se modifico correctamente", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("usuarios");
        try {
            for (int i=0;i<json.length();i++){
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                txtTelefono.setText(jsonObject.optString("0"));
                txtMovil.setText(jsonObject.optString("1"));
                txtCorreo.setText(jsonObject.optString("2"));
                txtCorreoAlt.setText(jsonObject.optString("3"));
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}